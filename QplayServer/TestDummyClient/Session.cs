﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace TestDummyClient
{
    public abstract class Session
    {
        Socket _socket;
        int _disconnected = 0;
        SocketAsyncEventArgs _sendArgs = new SocketAsyncEventArgs();
        SocketAsyncEventArgs _recvArgs = new SocketAsyncEventArgs();
        object _lock = new object();
        Queue<ArraySegment<byte>> _sendQueue = new Queue<ArraySegment<byte>>();
        List<ArraySegment<byte>> _pendinglist = new List<ArraySegment<byte>>();

        // RecvBuffer는 내부에 있으면서 SendBuffer는 외부에서 정의하는 이유
        /*
            Network에서 가장 이슈가 되는 부분은 Send/Recv 할 때이다.
            Recv와는 다르게 Send는 컨텐츠 단에서 다양한 방식으로 이용될 수 있다. (같은 존 내의 플레이어에게 패킷전송등..)
            이러한 상황에서 내부에 SendBuffer를 두고 관리를하게되면 패킷을 보내야하는 수만큼 copy가 일어나니 효율적이지못함
            따라서 외부에서 조립해 보내주면 불필요한 복사를 막을수 있다.
         */
        RecvBuffer _recvBuffer = new RecvBuffer(1024);

        public abstract void OnConnected(EndPoint ep);

        public abstract int OnRecv(ArraySegment<byte> buffer);

        public abstract void OnSend(int numBytes);


        public abstract void OnDisconnected(EndPoint ep);
        
        public void Start(Socket socket)
        {
            _socket = socket;
            

            // event (recv)가 완료되면 OnRecvCompleted를 수행
            // 정확히는 RegisterRecv에서 ReceiveAsync가 pending을 발생시키면 기다렸다가 완료되면 콜백으로 다시 수행
            _recvArgs.Completed += new EventHandler<SocketAsyncEventArgs>(OnRecvCompleted);
            _sendArgs.Completed += new EventHandler<SocketAsyncEventArgs>(OnSendCompleted);
           
            RegisterRecv();
        }
      
        public void Send(ArraySegment<byte> sendBuffer)
        {
                _sendQueue.Enqueue(sendBuffer);
                 RegisterSend();
        }

        public void DisConnect()
        {
            // disconnect가 두 번 발생한다면?
            if (Interlocked.Exchange(ref _disconnected, 1) == 1)
                return;
            OnDisconnected(_socket.RemoteEndPoint);
            _socket.Shutdown(SocketShutdown.Both);
            _socket.Close();
        }

        #region 네트워크 통신

        void RegisterSend()
        {

            ArraySegment<byte> segment = _sendQueue.Dequeue();
            _sendArgs.SetBuffer(segment.Array, segment.Offset, segment.Count);
         
            bool pending = _socket.SendAsync(_sendArgs);
            if (pending == false)
            {
                OnSendCompleted(null, _sendArgs);
            }
        }

       
        void OnSendCompleted(object sender, SocketAsyncEventArgs args)
        {
           
                if (args.BytesTransferred > 0 && (args.SocketError == SocketError.Success))
                {
                    OnSend(_sendArgs.BytesTransferred);
                }
                else
                {
                    DisConnect();
                }
          
           
        }
        void RegisterRecv()
        {
            _recvBuffer.Clean();
            ArraySegment<byte> segment =  _recvBuffer.WriteSegment;
           
            // 유저 버퍼를 os에 넘겨줌
            _recvArgs.SetBuffer(segment.Array, segment.Offset, segment.Count);

            bool pending = _socket.ReceiveAsync(_recvArgs);
            if(pending == false) 
            {
                OnRecvCompleted(null, _recvArgs);
            }
        }

       

        void OnRecvCompleted(object sender, SocketAsyncEventArgs args)
        {
            if(args.BytesTransferred > 0 && (args.SocketError == SocketError.Success ))
            {
                
                if (_recvBuffer.OnWrite(args.BytesTransferred) == false)
                {
                  
                    DisConnect();
                    return;
                }
               
                int processLen = OnRecv(_recvBuffer.ReadSegment);
                // TODO : processLen과 ByteTransferred가 같은지 확인
                //if (processLen < 0 || _recvBuffer.DataSize <processLen)
                //{
                //    DisConnect();
                //    return;
                //}

                if(_recvBuffer.OnRead(processLen) == false)
                {
                    DisConnect();
                    return;
                }
                RegisterRecv();
            }
            else
            {
                DisConnect();
            }
        }
        #endregion
    }
}
