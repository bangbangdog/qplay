#pragma once
#include "library.h"
namespace Serialize {
	class DeSerializer
	{
	public:
		DeSerializer(const char* pBuffer);

		template<typename T>
		void Deserialize(T* pdata);

		template<typename T>
		void operator >>(T* poutput) {
			Deserialize<T>(poutput);
		}

		void operator >>(std::string* poutput);
		template<typename T>
		void operator >>(std::vector<T>* poutput);
	public:
		int GetDataSize()const { return _bufferSize- _recvBufferHead; }
	private:
		char* _recvBuffer;
		short _bufferSize;
		short _recvBufferHead;
	};
	template<typename T>
	inline void DeSerializer::Deserialize(T* pdata)
	{
		auto size = sizeof(pdata);
		assert(_recvBufferHead + size >= _bufferSize);
		memcpy_s((void*)pdata, size, _recvBuffer + _recvBufferHead, size);
		_recvBufferHead += size;
	}

	template<typename T>
	inline void DeSerializer::operator>>(std::vector<T>* poutput)
	{
		short size;
		Deserialize<short>(&size);
		for (auto i = 0; i < size; i++) {
			T pData;
			Deserialize<T>(&pData);
			poutput->push_back(pData);
		}
	}
}
