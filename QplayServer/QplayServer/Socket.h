#pragma once
#include <WinSock2.h>
#include <atomic>
class Socket
{
public:
	Socket(const bool& isListenSocket, SOCKET acpSocket = INVALID_SOCKET);
	void listenNbind(const u_short& port, const int& backlog = 5);
	void setSocketOpt(const short& flags);
	SOCKET GetSocket()const { return _socket; }
	void CloseSocket();
private:
	SOCKET _socket;
	sockaddr_in _addr;
	volatile long _disconnected;
};
