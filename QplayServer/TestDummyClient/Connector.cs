﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace TestDummyClient
{
    class Connector
    {
        Func<Session> _sessionFactory;
        public void Connect(IPEndPoint ep, Func<Session> sessionFactory)
        {
            Socket socket = new Socket(ep.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            SocketAsyncEventArgs connectArgs = new SocketAsyncEventArgs();
            connectArgs.Completed += new EventHandler<SocketAsyncEventArgs>(OnConnectCompleted);
            connectArgs.RemoteEndPoint = ep;
            connectArgs.UserToken = socket;
            _sessionFactory = sessionFactory;
            RegisterConnect(connectArgs);
        }

        void RegisterConnect(SocketAsyncEventArgs args)
        {
            Socket socket = args.UserToken as Socket;
            if (socket == null) return;

            bool pending = socket.ConnectAsync(args);
            if (pending == false)
                OnConnectCompleted(null, args);
        }

        void OnConnectCompleted(object sender, SocketAsyncEventArgs args)
        {
            if(args.SocketError == SocketError.Success)
            {
                Session session = _sessionFactory.Invoke();
                session.Start(args.ConnectSocket);
                session.OnConnected(args.RemoteEndPoint);
            }
            else
            {
                Console.WriteLine($"OnConnectCompleted Failed! : {args.SocketError.ToString()}");
            }
        }
    }
}
