#pragma once
#include "library.h"
class PacketManager
{
public:
	void InitPakcet();
	void AddPakcet(const short& pkid, std::function<void()> _func);
	void ProcessPacket(const short& pkid);

private:
	std::unordered_map<const short, std::function<void()>> _packet;
};

