#include "IOCPserver.h"
#include <process.h>
#include "library.h"
#include "ErrorLog.h"
#include "Socket.h"
#include "Session.h"
#include "QuizParser.h"
#include "Packet.h"
#pragma comment (lib,"ws2_32.lib")

void IOCPserver::Run(const u_short& port)
{
	_isRun = true;
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
		PrintSocketErrorCode();
		return;
	}

	_listenSocket = new Socket(true);
	_listenSocket->listenNbind(port);

	_iocpHandle = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);
	if (!_iocpHandle) {
		PrintSocketErrorCode();
		return;
	}

	SYSTEM_INFO sysinfo;
	GetSystemInfo(&sysinfo);
	int threadcount = sysinfo.dwNumberOfProcessors * 2;

	for (int i = 0; i < threadcount; i++) {
		// 워커 스레드 생성후 일감 던지기
		_workerThreads.emplace_back([&]() {WorkingThread(); });
	}

	// Quiz Initialize
	quizParser = new QuizParser();
	quizParser->InitQuiz("quiz.json");
	//quizParser->viewQuizFullText();

	// Server Loop Start!
	AcceptThread();
}

IOCPserver::~IOCPserver()
{
	for (auto iter = _workerThreads.begin(); iter != _workerThreads.end(); ++iter) {
		iter->join();
	}
	_workerThreads.clear();
	_acceptThread.join();
	_listenSocket->CloseSocket();
	WSACleanup();
}

void IOCPserver::WorkingThread()
{
	// 일을 한다?  GQCS를 통해서 ( handle은 멤버변수 handle!)

	DWORD readn;
	DWORD flags = 0;
	Session* sInfo = NULL;
	SessionOverlapped* overlapped = NULL;
	while (_isRun) {
		auto ret = GetQueuedCompletionStatus(_iocpHandle, &readn, (PULONG_PTR)&sInfo, (LPOVERLAPPED*)&(overlapped), INFINITE);
		if (ret == false) {
			CloseSesssion(sInfo);
			return;
		}
		// 읽을 데이터가 없다! - 연결 종료의 의미인가?
		if (readn == 0) {
			CloseSesssion(sInfo);
			return;
		}
		SessionOverlapped* poverlapped = (SessionOverlapped*)overlapped;
		// Send,Recv가 들어왔을 때 어떻게 해석할 것인가?
		// Packet을 어떻게 분석할지, 분석하는 클래스를 만들어 일처리를 맡기자!
		switch (poverlapped->iodir) {
		case IODIR::RECV:
			
			poverlapped->buf[readn] = NULL;

			printf("[Recv] byte : %d , msg : %s\n", readn, poverlapped->buf);
			if (SendMsg(sInfo, poverlapped->buf, readn))
				RecvSession(sInfo);
			break;
		case IODIR::SEND:
			poverlapped->buf[readn] = NULL;
			printf("[Send] byte : %d , msg : %s\n", readn, poverlapped->buf);
			break;
		default:
			std::cout << "WorkingThread IODIR Exception" << std::endl;
			break;
		}

		ZeroMemory(overlapped, sizeof(overlapped));
	}
}

void IOCPserver::AcceptThread()
{
	// while문으로 돌면서 클라이언트를 받을 준비를 한다.
	// 클라이언트 관련 정보가 필요함 - 세션정보! 클라-서버가 언어가 달라서 공통 구조체 구현은 불가능

	sockaddr_in addr;
	ZeroMemory(&addr, sizeof(sockaddr_in));
	int addr_len = sizeof(sockaddr_in);

	_sessionCount = 0;

	while (_isRun) {
		SOCKET acp = accept(_listenSocket->GetSocket(), (SOCKADDR*)&addr, &addr_len);
		if (acp == INVALID_SOCKET) {
			PrintSocketErrorCode();
			return;
		}
		Socket client(false, acp);
		Session* session = new Session(client, _sessionCount);

		HANDLE ret = CreateIoCompletionPort((HANDLE)client.GetSocket(), _iocpHandle, (ULONG_PTR)session, 0);
		if (ret == NULL || ret != _iocpHandle) {
			PrintSocketErrorCode();
			continue;
		}

		// recv를 해야 그다음 send할게 생긴다! (만약 연결되자마자 보낼 메시지가 있다면 코드가 달라질 것)
		bool brecv = RecvSession(session);
		if (brecv == false) {
			return;
		}

		char clientIP[32] = { 0, };
		// binary ip -> text ip 교환
		inet_ntop(AF_INET, (const void*)&addr, clientIP, 32 - 1);

		memcpy_s(session->_ip, sizeof(session->_ip), clientIP, sizeof(clientIP));

		_idx_sessionMap[_sessionCount] = session;

		std::cout << session->_connectDate << std::endl;
		std::cout << "[IP] " << session->_ip << " [Idx] " << session->_sessionID << " Connected!" << std::endl;
		_sessionCount++;
	}
}

bool IOCPserver::RecvSession(Session* session)
{
	// wsarecv 형태로 받으면됨
	DWORD dwflag = 0;
	DWORD dwRecvNumBytes = 0;
	ZeroMemory(session->_recvOverlapped.buf, sizeof(session->_recvOverlapped.buf));

	session->_recvOverlapped.wsabuf.len = 1024; // TODO : 패킷 사이즈 결정
	session->_recvOverlapped.wsabuf.buf = session->_recvOverlapped.buf;
	session->_recvOverlapped.iodir = IODIR::RECV;

	int ret = WSARecv(session->_socket->GetSocket(), &(session->_recvOverlapped.wsabuf),
		1, &dwRecvNumBytes, &dwflag, (LPOVERLAPPED) & (session->_recvOverlapped), NULL);

	if (ret == SOCKET_ERROR && (WSAGetLastError() != WSA_IO_PENDING)) {

		PrintSocketErrorCode();
		return false;
	}

	return true;
}

bool IOCPserver::SendMsg(Session* session, char* pMsg, int nLen)
{
	DWORD dwRecvNumBytes = 0;

	CopyMemory(session->_sendOverlapped.buf, pMsg, nLen);
	session->_sendOverlapped.buf[nLen] = '\0';
	session->_sendOverlapped.wsabuf.buf = session->_sendOverlapped.buf;
	session->_sendOverlapped.wsabuf.len = nLen;
	session->_sendOverlapped.iodir = IODIR::SEND;

	int ret = WSASend(session->_socket->GetSocket(), &(session->_sendOverlapped.wsabuf), 1, &dwRecvNumBytes, 0,
		(LPWSAOVERLAPPED) & (session->_sendOverlapped), NULL);
	if (ret == SOCKET_ERROR && (WSAGetLastError() != WSA_IO_PENDING)) {
		
		PrintSocketErrorCode();
		return false;
	}

	return true;
}

void IOCPserver::CloseSesssion(Session* session)
{
	
	session->_socket->CloseSocket();
	auto iter = _idx_sessionMap.find(session->_sessionID);
	if (iter != _idx_sessionMap.end()) {
		_idx_sessionMap.erase(iter);
	}
	_sessionCount--;

	delete session;
}