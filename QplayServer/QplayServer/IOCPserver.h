#pragma once
#include "library.h"
class Socket;
class Session;
class QuizParser;
class IOCPserver
{
public:
	void Run(const u_short& port);
	~IOCPserver();
private:
	void WorkingThread();
	void AcceptThread();
	bool RecvSession(Session* session);
	bool SendMsg(Session* session, char* pMsg, int nLen);
	void CloseSesssion(Session* session);

private:
	Socket* _listenSocket;
	HANDLE _iocpHandle;
	QuizParser* quizParser;
	int _sessionCount;
	std::deque<std::thread> _workerThreads;
	std::unordered_map<int, Session*> _idx_sessionMap;
	std::thread _acceptThread;
	bool _isRun;
};
