#pragma once
#include <iostream>
#include <WinSock2.h>
inline void PrintSocketErrorCode() {
	
	LPVOID lpMsgBuf; // 오류나면 메세지를 띄운다.
	char* msg = nullptr;
	/* FORMAT_MESSAGE_ALLOCATE_BUFFER는 오류 메세지를 저장할 공간을 FormatMessage()함수가
	알아서 할당 FORMAT_MESSAGE_FROM_SYSTEM은 운영체제로 부터 오류 메세지를 가져와서 띄움 */
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL,
		WSAGetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		// MAKELANGID는 제어판에 설정된 기본언어로 오류메세지 띄움
		(LPTSTR)&lpMsgBuf, 0, NULL);
	MessageBox(NULL, (LPCTSTR)lpMsgBuf, (LPCWSTR)msg, MB_ICONERROR);
	LocalFree(lpMsgBuf);
	// 시스템에 할당된 메모리 반환
	exit(1); // 빠져나옴
}
