#pragma once
#define PACKET_SIZE 1024
struct PacketHeader {
	short id;
	short size;
};


struct TESTPacket : public PacketHeader {
	int test;
};