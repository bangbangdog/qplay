#include "DeSerializer.h"

namespace Serialize {
	DeSerializer::DeSerializer(const char* pBuffer)
	{
		int bufferSize = sizeof(pBuffer);
		_bufferSize = bufferSize;
		_recvBufferHead = 0;
		_recvBuffer = new char[bufferSize];
		memcpy_s(_recvBuffer, bufferSize, pBuffer, bufferSize);
	}

	void DeSerializer::operator>>(std::string* poutput)
	{
		short count = 0;
		Deserialize(&count);
		for (auto i = 0; i < count; i++) {
			char data;
			Deserialize(&data);
			poutput->push_back(data);
		}
	}
}