﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace TestDummyClient
{
    public abstract class Packet
    {
        public ushort size;
        public ushort packetID;

        public abstract ArraySegment<byte> Serialize();
        public abstract void DeSerialize(ArraySegment<byte> s);
    }
   
    public class PlayerInfoReq : Packet
    {
        public long playerID;
        public string name;
        public PlayerInfoReq()
        {
            this.packetID = (ushort)PacketID.PlayerInfoReq;
            this.name = "sanghun";

        }
        public override void DeSerialize(ArraySegment<byte> segment)
        {
            ushort count = 0;
            ReadOnlySpan<byte> s = new ReadOnlySpan<byte>(segment.Array, segment.Offset, segment.Count);
            count += sizeof(ushort);
            count += sizeof(ushort);
            this.playerID = BitConverter.ToInt64(s.Slice(count,s.Length-count));
            count += sizeof(long);

            ushort nameLen = BitConverter.ToUInt16(s.Slice(count, s.Length - count));
            count += sizeof(ushort);
            name = Encoding.Unicode.GetString(s.Slice(count, nameLen));
        }

        public override ArraySegment<byte> Serialize()
        {
            
            ArraySegment<byte> segment = SendBufferHelper.Open(4096);
            Span<byte> s = new Span<byte>(segment.Array,segment.Offset,segment.Count);
      
            bool success = true;
            ushort count = 0;

            count += sizeof(ushort);
            success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.packetID);
            count += sizeof(ushort);
            success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), this.playerID);
            count += sizeof(long);

            ushort nameLen = (ushort)Encoding.Unicode.GetByteCount(this.name);
            success &= BitConverter.TryWriteBytes(s.Slice(count, s.Length - count), nameLen);
            count += sizeof(ushort);
            Array.Copy(Encoding.Unicode.GetBytes(this.name),0,segment.Array,count,nameLen);
            count += nameLen;

            success &= BitConverter.TryWriteBytes(s, count);
            

            if (success == false)
                return null;
            return SendBufferHelper.Close(count);
        }
    }

 
    public enum PacketID
    {
        PlayerInfoReq = 1,
        PlayerInfoOk = 2,
    }
    class ServerSession : Session
    {
       
        public override void OnConnected(EndPoint ep)
        {
            Console.WriteLine($"[OnConnected] : {ep.ToString()}");
            for(int i =0; i<10; i++)
            {
                PlayerInfoReq packet = new PlayerInfoReq() { packetID = 10, size = 12, playerID = 1001 };

                ArraySegment<byte> segment = packet.Serialize();
                if (segment != null)
                    Send(segment);
                Thread.Sleep(1000);
            }
           

        }

        public override void OnDisconnected(EndPoint ep)
        {
            Console.WriteLine($"[OnDisconnected] : {ep.ToString()}");
        }


        public override void OnSend(int numBytes)
        {
            Console.WriteLine($"Transferred Bytes : {numBytes}");
        }

        /* TODO : PKID를 통해 어떤 클래스를 생성해서 데이터를 받을건지 (PK_MANAGER를 통해서 PKID 크기만큼 왔는지, 
           PK 크기만큼 왔는지 확인하고 두 조건 만족 됐을 때 패킷을 처리한다.
         */
        public override int OnRecv(ArraySegment<byte> buffer)
        {
            
            PlayerInfoReq packet = new PlayerInfoReq();
            int size = 0;
            packet.size = BitConverter.ToUInt16(buffer.Array, buffer.Offset);
            size += 2;
            packet.packetID = BitConverter.ToUInt16(buffer.Array, buffer.Offset + size);
            size += 2;
            packet.playerID = BitConverter.ToUInt32(buffer.Array, buffer.Offset + size);
            size += 8;

            Console.WriteLine($"size : {packet.size}");
            Console.WriteLine($"packetid : {packet.packetID}");
            Console.WriteLine($"playerid : {packet.playerID}");

            return size;
        }
    }
}
