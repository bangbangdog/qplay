#pragma once
#include "library.h"
namespace Serialize {
	class Serializer
	{
	public:
		Serializer(const short& bufferSize);

		template<typename T>
		void Serialize(const T& data);

		template<typename T>
		Serializer* operator << (const T& input);

		Serializer* operator << (const std::string& input);
		template<typename T>
		Serializer* operator << (const std::vector<T>& input);
	public:
		int GetBufferSize()const { return _bufferSize; }
		int GetDataSize()const { return _sendBufferHead; }
		int GetRemainedSize()const { return _bufferSize - _sendBufferHead; }
	private:
		char* _sendBuffer;
		short _bufferSize;
		short _sendBufferHead;
	};

	template<typename T>
	inline void Serializer::Serialize(const T& data)
	{
		auto dataSize = sizeof(data);
		assert(_sendBufferHead + dataSize < _bufferSize);
		memcpy_s(_sendBuffer + _sendBufferHead, dataSize, (void*)&data, dataSize);
		_sendBufferHead += dataSize;
	}

	template<typename T>
	inline Serializer* Serializer::operator<<(const T& input)
	{
		Serialize<T>(input);
		return this;
	}

	template<typename T>
	inline Serializer* Serializer::operator<<(const std::vector<T>& input)
	{
		short count = input.size();
		Serialize<short>(count);
		for (int i = 0; i < input.size(); i++) {
			Serialize<T>(input[i]);
		}
		return this;
	}
}
