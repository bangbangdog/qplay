﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace TestDummyClient
{
 
    class Program
    {
        static void Main(string[] args)
        {

            var host = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress addr = null;
           
            // c++쪽에서 ipv4만을 받기때문에 ipv4 형식으로 주소를 기입해야함
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    addr = ip;
                }
            }
            if(addr == null)
            {
                Console.WriteLine("ipaddress is null");
                return;
            }

            IPEndPoint endPoint = new IPEndPoint(addr, 32000);
            Connector connector = new Connector();

            // 다양한 세션이 있을수있다는 가정 (프로젝트에서는 서버 세션 하나)
            connector.Connect(endPoint, () => { return new ServerSession(); });
            while (true)
            {
                try
                {

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
           
   
        }
    }
}
