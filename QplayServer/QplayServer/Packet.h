#pragma once
#include "Serializer.h"
#include "DeSerializer.h"
#include "PacketInfo.h"
using namespace Serialize;



class Packet
{
public:
	template<typename T>
	Packet* operator << (T& input);
	template<typename T>
	void operator >> (T* output);
public:
	int GetPakcetSize()const { return _packetSize; }
	// TODO : PKID - Enum Class로 변환후 Get할 수 있도록


protected:
	PacketHeader _pkHeader;
	Serialize::Serializer* _serializer;
	Serialize::DeSerializer* _deserializer;
	int _packetSize;
};

template<typename T>
inline Packet* Packet::operator<<(T& input)
{
	if (_serializer == nullptr) {
		std::cout << "Packet Serializer is NULL!" << std::endl;
		return nullptr;
	}

	*_serializer << input;
	_packetSize = *_serializer.GetDataSize();
	return this;
}

template<typename T>
inline void Packet::operator>>(T* output)
{
	if (_deserializer == nullptr) {
		std::cout << "Packet Deserializer is NULL!" << std::endl;
		return;
	}
	_packetSize = *_deserializer.GetDataSize();
	_deserializer >> output;
}


/*
	TODO : 이런식으로 패킷을 상속받아 사용하면 Read(),Write() 함수를 통해서 
*/

class TestPacket : public Packet {
private:

public:
	TestPacket(const unsigned short& pkID, const unsigned short& pkSize);

};
