#include "Serializer.h"

namespace Serialize {
	Serializer::Serializer(const short& bufferSize)
	{
		_sendBuffer = new char[bufferSize];
		_sendBufferHead = 0;
		_bufferSize = bufferSize;
	}

	Serializer* Serializer::operator<<(const std::string& input)
	{
		size_t count = input.size();
		Serialize<size_t>(count);
		for (size_t i = 0; i < input.size(); i++) {
			Serialize<char>(input[i]);
		}
		
		return this;
	}
}