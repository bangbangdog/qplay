#include "QuizParser.h"
#include <iostream>
#include <fstream>
#include <locale>
#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include "Random.h"
using namespace rapidjson;

// rapidJson Type Mapping
static const char* kTypeNames[] =
{ "Null", "False", "True", "Object", "Array", "String", "Number" };

static const char* quizParser[] =
{ "id","type","content","answer" };

void QuizParser::InitQuiz(std::string fileName)
{
	std::locale::global(std::locale("ko_KR.UTF-8")); // 한글 깨짐 방지
	BindQuizType();

	FILE* fp = fopen(fileName.c_str(), "rb");
	char readbuffer[65536];
	FileReadStream is(fp, readbuffer, sizeof(readbuffer));
	Document d;
	d.ParseStream(is);

	quizCount = d.Size();
	for (SizeType i = 0; i < d.Size(); i++) {
		Quiz q;
		q.id = std::stoi(d[i][quizParser[(int)QUIZ_TYPE::ID]].GetString());
		q.category = GetStringToCategory(d[i][quizParser[(int)QUIZ_TYPE::CATEGORY]].GetString());
		q.content = d[i][quizParser[(int)QUIZ_TYPE::CONTENT]].GetString();
		q.answer = d[i][quizParser[(int)QUIZ_TYPE::ANSWER]].GetString();

		id_Quiz[i] = q;
	}

	fclose(fp);
}

std::vector<Quiz> QuizParser::GetRandomQuizBundle(const int& maxSize)
{
	std::unordered_map<int, bool> indexes;
	std::vector<Quiz> bundle;
	for (int i = 0; i < maxSize; i++) {
		while (true) {
			int idx = GetRand(0, quizCount - 1);
			if (indexes.find(idx) == indexes.end()) {
				if (indexes[i]) continue;

				indexes[i] = true;
				bundle.push_back(id_Quiz[idx]);
				break;
			}
			else {
				continue;
			}
		}
	}
	return bundle;
}

void QuizParser::viewQuizFullText()
{
	for (auto iter : id_Quiz) {
		std::cout << "id : " << iter.first << std::endl;
		std::cout << " - " << "category : " << GetCategoryToString(iter.second.category) << std::endl;
		std::cout << " - " << "content : " << iter.second.content << std::endl;
		std::cout << " - " << "answer : " << iter.second.answer << std::endl;
	}
}

CATEGORY QuizParser::GetStringToCategory(const std::string& str)
{
	auto iter = str_categoryQuiz.find(str);
	if (iter != str_categoryQuiz.end()) {
		return str_categoryQuiz[str];
	}
	return CATEGORY::NONE;
}

std::string QuizParser::GetCategoryToString(const CATEGORY& category)
{
	auto iter = category_strQuiz.find(category);
	if (iter != category_strQuiz.end()) {
		return category_strQuiz[category];
	}
	// TODO : 에러 처리를 어떻게 할까요.. ?
}

void QuizParser::BindQuizType()
{
	str_categoryQuiz["society"] = CATEGORY::SOCIETY;
	str_categoryQuiz["economy"] = CATEGORY::ECONOMY;
	str_categoryQuiz["knowledge"] = CATEGORY::KNOWLEDGE;
	str_categoryQuiz["history"] = CATEGORY::HISTORY;
	str_categoryQuiz["commonsense"] = CATEGORY::COMMONSENSE;
	str_categoryQuiz["animation"] = CATEGORY::ANIMATION;
	str_categoryQuiz["character"] = CATEGORY::CHARACTER;
	str_categoryQuiz["nation"] = CATEGORY::NATION;
	str_categoryQuiz["novel"] = CATEGORY::NOVEL;
	str_categoryQuiz["science"] = CATEGORY::SCIENCE;
	str_categoryQuiz["nonsense"] = CATEGORY::NONSENSE;

	category_strQuiz[CATEGORY::SOCIETY] = "society";
	category_strQuiz[CATEGORY::ECONOMY] = "economy";
	category_strQuiz[CATEGORY::KNOWLEDGE] = "knowledge";
	category_strQuiz[CATEGORY::HISTORY] = "history";
	category_strQuiz[CATEGORY::COMMONSENSE] = "commonsense";
	category_strQuiz[CATEGORY::ANIMATION] = "animation";
	category_strQuiz[CATEGORY::CHARACTER] = "character";
	category_strQuiz[CATEGORY::NATION] = "nation";
	category_strQuiz[CATEGORY::NOVEL] = "novel";
	category_strQuiz[CATEGORY::SCIENCE] = "science";
	category_strQuiz[CATEGORY::NONSENSE] = "nonsense";
}