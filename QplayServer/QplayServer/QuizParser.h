#pragma once
#include <string>
#include <unordered_map>
#include <vector>

enum class CATEGORY {
	NONE,
	COMMONSENSE,
	HISTORY,
	CHARACTER,
	SOCIETY,
	ECONOMY,
	NATION,
	SCIENCE,
	KNOWLEDGE,
	NOVEL,
	ANIMATION,
	NONSENSE,
	END
};

struct Quiz {
	int id;
	CATEGORY category;
	std::string content;
	std::string answer;
};

enum class QUIZ_TYPE {
	ID,
	CATEGORY,
	CONTENT,
	ANSWER,
	END
};

class QuizParser
{
public:
	void InitQuiz(const std::string fileName);
	// TODO : 다른 클래스에서 구현하자!
	std::vector<Quiz> GetRandomQuizBundle(const int& maxSize = 10);
	void viewQuizFullText();
private:
	// category type -> string , string -> category type
	void BindQuizType();
	// TODO : 다른 클래스에서 구현하자!
	CATEGORY GetStringToCategory(const std::string& str);
	std::string GetCategoryToString(const CATEGORY& category);

public:
	std::unordered_map<int, Quiz> id_Quiz;
	std::unordered_map<CATEGORY, std::vector<Quiz>> category_Quiz; // 카테고리별 퀴즈
	// string - category mapping
	std::unordered_map<std::string, CATEGORY> str_categoryQuiz;
	std::unordered_map<CATEGORY, std::string> category_strQuiz;
private:
	int quizCount;
};
