#pragma once
#include <iostream>
#include <random>

inline int GetRand(const int& min, const int& max)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<int> dis(min, max);
	return dis(gen);
}