#include "Socket.h"
#include "library.h"
#include <WS2tcpip.h>

Socket::Socket(const bool& isListenSocket, SOCKET acpSocket)
{
	memset(&_addr, 0, sizeof(_addr));
	// TCP만 쓴다고 가정하자!
	if (isListenSocket) {
		_socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, NULL, WSA_FLAG_OVERLAPPED);
		if (_socket == INVALID_SOCKET) {

			PrintSocketErrorCode();
		}
	}
	else {
		_socket = acpSocket;
		if (_socket == INVALID_SOCKET) {
			PrintSocketErrorCode();
		}
	}
	_disconnected = 0;
}

void Socket::listenNbind(const u_short& port, const int& backlog)
{
	_addr.sin_family = AF_INET;
	_addr.sin_port = htons(port);
	_addr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	/*inet_pton(AF_INET, "localhost", &(_addr.sin_addr));*/

	if (bind(_socket, (const sockaddr*)&_addr, sizeof(sockaddr)) == SOCKET_ERROR) {
		PrintSocketErrorCode();
	}

	if (listen(_socket, backlog) == SOCKET_ERROR) {
		PrintSocketErrorCode();
	}
}

void Socket::setSocketOpt(const short& flags)
{
}

void Socket::CloseSocket()
{
	
	if (InterlockedExchange(&_disconnected, 1) == 1)
		return;
	shutdown(_socket, SD_BOTH);
	closesocket(_socket);
	
}