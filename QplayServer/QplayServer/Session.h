#pragma once
#include "library.h"
enum class IODIR {
	SEND,
	RECV,
};

struct SessionOverlapped {
	OVERLAPPED overlapped;
	WSABUF wsabuf;
	IODIR iodir;
	char buf[1024]; //TODO : 나중에 패킷 사이즈 정해서 넣자!
};

class Socket;
class Session
{
public:
	bool _isconnect;
	int _sessionID;
	std::string _connectDate;
	Socket* _socket;
	SessionOverlapped _recvOverlapped;
	SessionOverlapped _sendOverlapped;
	char _ip[32];
	Session(Socket& socket, const int& sessionid);
	~Session();
};
