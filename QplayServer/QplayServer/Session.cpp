#include "Session.h"
#include <time.h>
#include "Socket.h"

Session::Session(Socket& socket, const int& sessionid)
{
	time_t now = time(0);
	tm tstruct;
	char buf[80];
	localtime_s(&tstruct, &now);
	strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

	_connectDate = buf;
	_isconnect = true;
	_sessionID = sessionid;
	_socket = &socket;

	// WSAGETLASTERROR : 6 INVALID_HANDLE 에러가 날 수 있다.
	ZeroMemory(&_recvOverlapped, sizeof(_recvOverlapped));
	ZeroMemory(&_sendOverlapped, sizeof(_sendOverlapped));
}

Session::~Session()
{
	std::cout << "[IP] : " << _ip << " [Idx] : " << _sessionID << " Close Session!" << std::endl;
}